<?php
/**
 * This file could be used to catch submitted form data. When using a non-configuration
 * view to save form data, remember to use some kind of identifying field in your form.
 */
?>
<script type="text/javascript">
	(function() {
		var url ="https://todoist.com/api/v8/sync"
		var data = {
			 token : "<?php echo self::get_dashboard_widget_option(self::wid, 'apiKey'); ?>",
			 sync_token : '*',
			 resource_types: '["projects"]'
		}

		jQuery.get(url,data,function(data,status,xhr){
			var treeElements = [];
			if (status == 'success'){
				var i = 0;
				var project;
				for (var i =0; i<data.projects.length; i++){
					project = data.projects[i];
					var treeElement = {
						id: project.id,
						text : project.name
					}
					treeElements.push(treeElement);
				}
				
      			jQuery('#projectTree').jstree({	
					'core' : {
						'data' : treeElements,
						'force_text' : true,
						'themes': {
							'variant': 'large'
						}
					},
					'checkbox': {
						'keep_selected_style': false,
						'tie_selection' : false,
						'whole_node': false,
						'tree_selection': false
					},
					'types' : {
						'#' : { 
							'max_children' : 1, 
							//'max_depth' : 4, 
							'valid_children' : ["root"] 
						},
						"root" : { 
							//"icon" : "/static/3.3.7/assets/images/tree_icon.png", 
							"valid_children" : ["default"] 
						},
						"default" : { 
							"valid_children" : ["default","file"] 
						},
						"file" : { 
							"icon" : "glyphicon glyphicon-file", "valid_children" : [] 
						}
					},
					'plugins': [ 'wholerow', 'checkbox']	
				}).on("select_node.jstree", function (e, data) {
					alert("node_id: " + data.node.id); 
				});;
				
			}
		},'json')
	})();
</script>

<div id="widget_for_todoist">
	<div id="todoist_header">
		<div id="todoist_header_logo">
			<img src="<?php echo plugin_dir_url( __FILE__ ) . 'images/Todoist-main_logo_positive.png'; ?>">
		</div>
		<div id="todoist_header_title">
			Wordpress todolist for <a href='http://www.todoist.com'>todoist</a>
		</div>
	</div>

	<div id="todoist_body">



		<p>Here is a list of all your projects: </p>
		<div id="projectTree" class="treeview"></div>

		<ul>
			<li><a href='https://developer.todoist.com/sync/v8/#get-item-info'>todoidt dev doc</a></li>
			<li><a href='http://jonmiles.github.io/bootstrap-treeview/'>bootrastrp tree view</a></li>
			<li><a href='https://codex.wordpress.org/Example_Dashboard_Widget'>create wp dashboard widget</a></li>
			<li><a href='https://www.jstree.com/docs/config/'>JStree</a></li>
		</ul>
	</div>
</div>

<!--
<p>This is the front-facing part of the widget, and can be found and edited from <tt><?php echo __FILE__ ?></tt></p>
<p>Widgets can be configured as well. Currently, this is set to <b><?php echo self::get_dashboard_widget_option(self::wid, 'apiKey'); ?></b> ! To change the number, hover over the widget title and click on the "Configure" link.</p>-->