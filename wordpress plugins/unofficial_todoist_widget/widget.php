<?php
/**
 * This file could be used to catch submitted form data. When using a non-configuration
 * view to save form data, remember to use some kind of identifying field in your form.
 */
?>
<script type="text/javascript">
	(function() {
		var widget_id = "<?php echo self::wid ?>";
		var nonce = "<?php echo wp_create_nonce( 'edit-dashboard-widget_' .self::wid ) ?>";
		var token = "<?php echo self::get_dashboard_widget_option(self::wid, 'apiKey'); ?>";
		var project_id = "<?php echo self::get_dashboard_widget_option(self::wid, 'project_id'); ?>";

		init(widget_id, nonce, token, project_id, '');
	})();
</script>

<div id="widget_for_todoist">

  <!-- for error messages -->
	<div id="todoist_error" class="todoist_content">
		<div class="todoist_header"></div>
		<div class="todoist_body"></div>
	</div>


  <!-- widget config -->
	<div id="todoist_config" class="todoist_content">
		<div class="todoist_header">Configuration</div>
		<div class="todoist_body">
			<div class="row">
				<img class="center" src="<?php echo plugin_dir_url( __FILE__ ) . 'images/config.png'; ?>"/>
			</div>
			<div class="row">
				<strong>Hmm.... that's embarassing.</strong><br/>
				You need an API-Key in order ti use this feature. You can find yours on your 
				<a href="https://todoist.com/prefs/integrations" target="_blank">todoist profile</a>
			</div>

			<div class="row">
			    <form id="todoist_config_form">
			        <input type="hidden" name="action" value="updateconfig" />
			        <input type="hidden" name="widget_id" value="<?php echo self::wid ?>">
			        <input type="hidden" name="project_id" value="">
			        <?php echo wp_nonce_field( 'edit-dashboard-widget_' .self::wid , 'dashboard-widget-nonce', true, false ) ?>

			        <span class="label">Add your API key here:</span>
			        <input id="todoist_config_form_apikey" type="text" name="apiKey" style="float: left"/>
			        <p class="submit">
			            <input type="submit" name="submit" id="submit" style="display:inline-block" class="button button-primary" value="Submit">
			        </p>
			    </form>
	    	</div>
	    	<div class="row">
			    Oh..by the way .. this is NOT an official todoist widget. Your checklists will be synchronized with your todoist account. 
			    You can find more information on <a href="https://todoist.com/overview">todoist website</a><br/> 
			</div>
		</div>
	</div>


  <!-- projects -->
	<div id="todoist_projects" class="todoist_content">
		<div class="todoist_header"></div>
		<div class="todoist_body"></div>
	</div>


  <!-- tasks -->
	<div id="todoist_tasks" class="todoist_content">
		<div class="todoist_header"></div>
		<div class="todoist_body"></div>
	</div>






<?php /*
	<div id="todoist_header">
		<!--<div id="todoist_header_logo">
			<img src="<?php echo plugin_dir_url( __FILE__ ) . 'images/Todoist-main_logo_positive.png'; ?>">
		</div>
		<div id="todoist_header_title">
			<h2>Your projects</h2>
		</div>-->

		<div id="todoist_header_title">
			Your Projects 
			<div id="show_add_project_button" onclick="show_header_form()">
				<span class="dashicons dashicons-plus-alt"></span>
			</div>
		</div>

		<div id="todoist_new_project">
			<span class="float-left label">Add project: </span>
			<form id="todoist_new_project_form">
				<input type="text" name="name" class="float-left"/>
	    		<p class="submit float-left">
	      			<input type="submit" name="submit" id="submit" style="display:inline-block" class="button button-primary" value="Submit">
		  		</p>
	  		</form>
		  	<a id="todoist_hide_header_form" href='#'>cancel</a>
			
			<div id="todoist_new_project_submit"></div>
		</div>
	</div>

	<div id="todoist_body">
		<div id="error"></div>

		<div id="projects_view">
			<div id="projectTree" class="treeview"></div>			
		</div>

		<div id="task_view"></div>


		<?php echo self::get_dashboard_widget_option(self::wid, 'apiKey'); ?>
		<ul>
			<li><a href='https://developer.todoist.com/sync/v8/#get-item-info'>todoidt dev doc</a></li>
			<li><a href='http://jonmiles.github.io/bootstrap-treeview/'>bootrastrp tree view</a></li>
			<li><a href='https://codex.wordpress.org/Example_Dashboard_Widget'>create wp dashboard widget</a></li>
			<li><a href='https://www.jstree.com/docs/config/'>JStree</a></li>
			<li><a href='https://developer.wordpress.org/resource/dashicons/#admin-settings'>Dashicons</a></li>
			<li><a href='https://rudrastyh.com/wordpress/dashboard-widgets-with-options.html'>Config as Ajax</a></li>
		</ul>
	</div>
	*/ ?>
</div>
