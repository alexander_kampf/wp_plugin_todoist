
/**--------------------------------------------------------------------------
	*
	*	file that handels the widget config
	*
----------------------------------------------------------------------------*/

/**
	* function that shows the config form
	*/
function config_form_show_hide(){
alert(jQuery('#todoist_config').is(':hidden'));

	if(jQuery('#todoist_config').is(":hidden") == true){
		jQuery('#todoist_config').css('display', 'inline-block');
		jQuery('#unofficial_todoist_widget .edit-box.open-box').html('close');
		jQuery('#todoist_config_form_apikey').val(todoist_APIkey);

	} else {
		jQuery('#todoist_config').css('display', 'none');
		jQuery('#unofficial_todoist_widget .edit-box.open-box').html('configure');
	}
}


/**
	*	function that updates the widget config
	*/
function config_form_send(){
}


