
/*-------------------------------------------------------------------------------------
    - manages global variables
    - file that initailises todoist API
    - init listener events
--------------------------------------------------------------------------------------*/

// includes js files for each section: widget config, projects, task, ...
//import TodoistAPI from '../node_modules/todoist-js';


/***
 * Global variables
 */
var todoist_widget_id;
var todoist_nonce;
var todoist_APIkey;
var todoist_projectID;
var todoist_taskId;
var todoist_api;




/**
 *  function that initializes global variables from php => js
 */
function init(pWidget_id, pNonce, pAPIkey, pProjectId, pTaskId) {

	// init global variables
	todoist_widget_id = pWidget_id;
	todoist_nonce = pNonce;
	todoist_APIkey = pAPIkey;
	todoist_projectID = pProjectId;
	todoist_taskId = pTaskId;

	// init todoist api

  // if api-key = empty => show config
  if (pAPIkey.length == 0 ){
  	config_form_show_hide();
  }
	// .. else if project id = empty => create project list
	// .. otherwise.. create task list for selected project
}


/**
 * function that initializes button event handlers
 */
 jQuery(function($){

 	// mfunction that opens the cwidget config
	jQuery('#unofficial_todoist_widget .edit-box.open-box').on('click',function(event){
		event.preventDefault();
		config_form_show_hide();
	});

	// function that saves the config
	jQuery('#todoist_config_form').on('submit', function(event){
		event.preventDefault();
		var data = $(this).serialize();
		if (update_todoist_config(data)){
			todoist_APIkey = jQuery('#todoist_config_form_apikey').val();
			config_form_show_hide();
		}

		return true;
	})
 });



 /**
 	* function that sends ajax request to wordpress 
 	* in order to update the widget config
 	*/
function update_todoist_config(data){
	jQuery.ajax(
	{	
		url: ajaxurl,
		type: 'POST',
		data: data
	}).done(function(data, status){
		return status === 'success';
	});
	
	return true;
} 	