/**--------------------------------------------------------------------------
	*
	*	file that handels the project list
	*
----------------------------------------------------------------------------*/

/**
	* function that shows the create new project form
	*/
function project_form_show(){
}


/**
	*	function that adds a new project 
	*/
function project_form_send(){
}


/**
	* function that hides the create new project form
	*/
function project_form_hide(){
}


/**
	*	function that initializes the project list
	*/
function project_list_init(){
}


/**
	*	function that deletes current project
	*/
function project_delete(){
}


/**
	*	function that updates a project
	*/
function project_update(){
}

