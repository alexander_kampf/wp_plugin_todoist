
var widget_id;
var nonce;
var token;
var sync_token;


/**
* init js global variables (from php)
*/
//function todoist_init(pWidget_id, pNonce, pToken){
//	widget_id = pWidget_id;
//	nonce = pNonce;
//	token = pToken;
//	sync_token = '';
//}



/**
* function that opens the config form 
*/
jQuery(function($){
	// the Configure link click event
	$('#unofficial_todoist_widget .edit-box.open-box').click(function(){
		var button = $(this);
		$.ajax({
			url: ajaxurl, // it is predefined in /wp-admin/
			type: 'POST',
			data: 'action=showconfig',
			beforeSend : function( xhr ){
				// add preloader
				button.hide().before('<span class="spinner" style="visibility:visible;display:block;margin:0 0 0 15px"></span>');
			},
			success : function( data ){
				// remove preloader
				button.prev().remove();
				// insert settings form
				$('#unofficial_todoist_widget').find('.inside').html(data);
			}
 
		});
		return false;
	});
 

	/*
	* gets the api key from config form and launches the saving  
	*/
	$('body').on('submit', '#setTodoistAPI', function(){
		var data = $(this).serialize();
		update_todoist_config(data);
		return true;
	});


/*
    -d token=0123456789abcdef0123456789abcdef01234567 \
    -d sync_token="aLGJg_2qwBE_kE3j9_Gn6uoKQtvQeyjm7UEz_aVwF8KdriDxw7e_InFZK61h" \
    -d resource_types='["projects"]' \
    -d commands='[
        { "type": "project_add",
          "temp_id": "24a193a7-46f7-4314-b984-27b707bd2331",
          "uuid": "e23db5ec-2f73-478a-a008-1cb4178d2fd1",
          "args": { "name": "Project1" } }
      ]'
      */

    /**
    * function that adds a new project 
    */  
	$('body').on('submit', '#todoist_new_project_form', function(){
		var form_data = jQuery(this).serialize();
		console.log (form_data);
		jQuery.ajax({
			url: "https://todoist.com/api/v8/sync",
			type: 'POST',
			data: {
				'token': token,
				'sync_token': sync_token,
				'resource_types': '["projects"]',
				'commands': [
					{
					
						'type': 'project_add',
						'name': form_data.name,
						'args': {
							'name': form_data.name
						}
					}	
				]

			},
			beforeSend : function( xhr ){
				// add preloader just after the submit button
				jQuery('#todoist_new_project_form').append('<span class="spinner" style="display:inline-block;float:none;visibility:visible;margin:0 0 0 15px"></span>');
			},
			success : function( data ){
				console.log(data);
				jQuery('.spinner').remove();
				jQuery('#todoist_new_project').hide();
				jQuery('#todoist_header_title').slideDown(300);
			},
			fail: function(data) {
				jQuery('.spinner').remove();
				jQuery('#todoist_new_project').hide();
				jQuery('#todoist_header_title').slideDown(300);
			}
		});


		return false;
	});



	/*
	* function that hides the title and shows the 'add new project ' form
	*/
	jQuery('#todoist_hide_header_form').on('click', function(event){
  		event.preventDefault();
		jQuery('#todoist_new_project').hide();
		jQuery('#todoist_header_title').slideDown(300);
	});

});



/**
* saves the config by ajax
*/
function update_todoist_config(data){
	jQuery.ajax({
		url: ajaxurl,
		type: 'POST',
		data: data, // all form fields
		beforeSend : function( xhr ){
			// add preloader just after the submit button
			jQuery('#unofficial_todoist_widget').find('.inside').append('<span class="spinner" style="display:inline-block;float:none;visibility:visible;margin:0 0 0 15px"></span>');
		},
		success : function( data ){
			jQuery('#unofficial_todoist_widget').find('.inside').html(data);
			// show the Configure link again
			jQuery('#unofficial_todoist_widget .edit-box.open-box').show();
		}
	});
	return false;
}



/**
* grap the project list and create project list
*/
function get_todoist_project_tree(widgetId, nonce, token){
	var url ="https://todoist.com/api/v8/sync"
	var data = {
		token : token,
		sync_token : '*',
		resource_types: '["projects"]'
	};

	jQuery.get(url,data,function(data,status,xhr){
		var treeElements = [];

		if (status == 'success'){
			sync_token = data.sync_token;
			var i = 0;
			var project;
			for (var i =0; i<data.projects.length; i++){
				project = data.projects[i];
				var treeElement = {
					id: project.id,
					text : project.name
				}
				treeElements.push(treeElement);
			}
			
			jQuery('#projectTree').jstree({	
				'core' : {
					'data' : treeElements,
					'force_text' : true,
					'themes': {
						'variant': 'large'
					}
				},
				'checkbox': {
					'keep_selected_style': false,
					'tie_selection' : false,
					'whole_node': false,
					'tree_selection': false
				},
				'types' : {
					'#' : { 
						'max_children' : 1, 
						//'max_depth' : 4, 
						'valid_children' : ["root"] 
					},
					"root" : { 
						//"icon" : "/static/3.3.7/assets/images/tree_icon.png", 
						"valid_children" : ["default"] 
					},
					"default" : { 
						"valid_children" : ["default","file"] 
					},
					"file" : { 
						"icon" : "glyphicon glyphicon-file", "valid_children" : [] 
					}
				},
				'plugins': [ 'wholerow', 'checkbox']	
			}).on("select_node.jstree", function (e, data) {
				//alert("node_id: " + data.node.id); 
				var widgetData = {
					'action': 'updateconfig',
					'widget_id': widgetId,
					'dashboard-widget-nonce': nonce,
					'project_id': data.node.id
				};
				update_todoist_config(widgetData);
			});

			jQuery('#detailview').show();
			jQuery('#error').hide();

		} else {

			jQuery('#detailview').hide();
			jQuery('#error').html(data);
			jQuery('#error').show();
		}
	},'json')
}


/**
* hides the header title and shows the new project form 
*/
function show_header_form(){
	jQuery('#todoist_header_title').hide();
	jQuery('#todoist_new_project').slideDown(300);
}


